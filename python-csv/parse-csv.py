import csv

# open then close csv file within context manager
with open('50-contacts.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    # write to a csv file
    with open('50-contacts.csv', 'w') as new_file:
        csv_writer = csv.writer('new_file', delimiter='-')
    # loop over the first line
    # step over first value 
    #next(csv_reader)

    # loop through lines starting from second line/value
    for line in csv_reader:
        print(line[-1])
