import csv

# open then close csv file within context manager
with open('50-contacts.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    # new context manager
    # create & write to a new csv file called new_names.csv
    with open('new_names.csv', 'w') as new_file:
        # write/use dashes as delimiter
        #csv_writer = csv.writer(new_file, delimiter='-')
        # write/use tab as delimiter
        csv_writer = csv.writer(new_file, delimiter='\t')


        # loop through lines starting from second line/value
        for line in csv_reader:
            csv_writer.writerow(line)
    
# open and read new created file 
with open('new_names.csv', 'r') as new_csv_file:
    # pass in delimiter to reader
    csv_reader = csv.reader(new_csv_file, delimiter='\t')

    for line in new_csv_file:
        print(line)