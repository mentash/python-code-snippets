import csv

# open then close csv file within context manager
with open('50-contacts.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    # loop over the first line
    # step over first value 
    next(csv_reader)

    # loop through lines starting from second line/value
    for line in csv_reader:
        # print emails [last index]
        print(line[-1])
