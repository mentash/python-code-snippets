import csv

# open then close csv file within context manager
with open('50-contacts.csv', 'r') as csv_file:
    # read file using the dictionary reader method
    csv_reader = csv.DictReader(csv_file)

    # loop over the first line
    # step over first value 
    next(csv_reader)

    # loop through lines starting from second line/value
    for line in csv_reader:
        # print each line as dictioanry with field names as keys
        #print(line)
        
        # print email - access email key
        print (line['email'])